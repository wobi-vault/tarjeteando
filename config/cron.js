module.exports.cron = {
  job: {
    schedule: '0 0 * * * *',
    onTick: function() {
      date = new Date(new Date().toLocaleString('es-419'));
      Document.send(date.getHours());
    }
  }
};
