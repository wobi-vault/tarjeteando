/**
 * ReportController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  email: function(req, res) {
    let params = req.allParams();
    var now = new Date();
    var yesterday =
      params.startDate ||
      new Date(
        now.getFullYear(),
        now.getMonth(),
        now.getDate() - 1,
        report.hour,
        0,
        0,
        0
      );
    var today =
      params.endDate ||
      new Date(
        now.getFullYear(),
        now.getMonth(),
        now.getDate(),
        report.hour,
        0,
        0,
        0
      );
    Document.excel(
      yesterday,
      today,
      params.columns,
      params.repeated,
      (err, data) => {
        if (err) {
          console.log(err);
        } else {
          var subject = params.subject || 'Reporte tarjeteando';
          var message = `Reporte del ${yesterday.toLocaleString('es')}
          al ${today.toLocaleString('es')}.
          Total de leads nuevos: ${data.count}.`;
          if (!params.repeated) {
            message =
              message +
              ' Nota: El reporte omite los leads repetidos, basado en su correo.';
          }
          var attachments =
            data.count > 0 ? [{
              path: data.path
            }] :
            null;
          Email.send(subject, message, attachments, params.emails, () => {
            Document.delete(data.path);
            return res.ok();
          });
        }
      }
    );
  },

  resend: async (req, res) => {
    let params = req.allParams();
    let report = await Report.findOne(params.id);
    var columns = report.columns.split(',').map(c => {
      return c.trim();
    });
    var now = new Date();
    var yesterday =
      params.startDate ||
      new Date(
        now.getFullYear(),
        now.getMonth(),
        now.getDate() - 1,
        report.hour,
        0,
        0,
        0
      );
    var today =
      params.endDate ||
      new Date(
        now.getFullYear(),
        now.getMonth(),
        now.getDate(),
        report.hour,
        0,
        0,
        0
      );
    Document.excel(
      yesterday,
      today,
      columns,
      report.repeatedData,
      (err, data) => {
        if (err) {
          console.log(err);
        } else {
          var subject = report.subject || 'Reporte ' + report.title;
          var message = `Reporte del ${yesterday.toLocaleString('es')}
          al ${today.toLocaleString('es')}.
          Total de leads nuevos: ${data.count}.`;
          if (!report.repeatedData) {
            message =
              message +
              ' Nota: El reporte omite los leads repetidos, basado en su correo.';
          }
          var attachments =
            data.count > 0 ? [{
              path: data.path
            }] :
            null;
          var emails = report.emails.split(',').map(c => {
            return c.trim();
          });
          Email.send(subject, message, attachments, emails, () => {
            Document.delete(data.path);
            return res.ok(report);
          });
        }
      }
    );
  },

  columns: (req, res) => {
    return res.ok(sails.config.report.columns);
  },

  download: function(req, res) {}
};