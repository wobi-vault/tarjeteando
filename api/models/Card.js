/**
 * Card.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    title: {
      type: 'string',
      required: true
    },
    description: {
      type: 'string'
    },
    image: {
      type: 'string',
      required: true
    },
    url: {
      type: 'string',
      required: true
    },
    order: {
      type: 'number',
      required: true
    },
    active: {
      type: 'boolean',
      defaultsTo: true
    }
  }
};
